import tweepy
import os

def create_api():
    # uncomment this part if you set keys and secrets as environmental variables
    # consumer_key = os.getenv("CONSUMER_KEY")
    # consumer_secret = os.getenv("CONSUMER_SECRET")
    # access_token = os.getenv("ACCESS_TOKEN")
    # access_token_secret = os.getenv("ACCESS_TOKEN_SECRET")

    # add your api key& secret and access token&  secret
    consumer_key = ''
    consumer_secret = ''
    access_token = ''
    access_token_secret = ''

    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)

    #creates an api with specified proxy(which is lantern's at the moment)
    api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True,proxy='127.0.0.1:42359')
    # verifying api
    try:
        api.verify_credentials()
        print("successfully connected")
    except Exception as e:
        print("connection failed")
        raise e

    return api
